package mx.unitec.moviles.practica5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.unitec.moviles.practica5.service.MyjobIntentService

class JobActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job)

        val intent = Intent(this, MyjobIntentService::class.java)
        intent.putExtra("name", 100)
        MyjobIntentService.enqueuework(this, intent)

    }
}